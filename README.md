# Movie List API with GraphQL
> Movie API with Graphql

### Spec
- Node.js 
- GraphQL Yoga
- [NOW Build Deploy](https://vercel.com/)

### Build Link (Deploy Server)
[https://movie-list-server-side.vercel.app/](https://movie-list-server-side.vercel.app/)

### Build (Local)
-```now dev```